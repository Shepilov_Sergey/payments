//
//  Date+Extension.swift
//  TestTask
//
//  Created by Sergey on 2/16/17.
//  Copyright © 2017 Sergey Shepilov. All rights reserved.
//

import Foundation

extension Date {
    
    func randomDateInYearOfDate() -> Date {
        let currentCalendar = Calendar.current
        var comps: DateComponents? = currentCalendar.dateComponents([.year, .month, .day], from: self)
        comps?.month = Int(arc4random_uniform(12))
        let range: Range = Calendar.current.range(of: .day, in: .month, for: currentCalendar.date(from: comps!)!)!
        comps?.day = Int(arc4random_uniform(UInt32(range.count)))
        comps?.hour = Int(arc4random_uniform(24))
        comps?.minute = Int(arc4random_uniform(60))
        comps?.second = Int(arc4random_uniform(60))
        comps?.timeZone = TimeZone(secondsFromGMT: 2)
        
        return currentCalendar.date(from: comps!)!
    }
}
