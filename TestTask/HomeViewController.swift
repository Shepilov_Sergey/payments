//
//  ViewController.swift
//  TestTask
//
//  Created by Sergey on 2/15/17.
//  Copyright © 2017 Sergey Shepilov. All rights reserved.
//

import UIKit

protocol PaymentHistory {
    func generateHistory() -> [Payment]
}

protocol AnimateTable {
    func animateTable() -> Void
}

class HomeViewController: UIViewController {
    @IBOutlet weak var balance: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let payButton = UIBarButtonItem(title: "Payment", image: #imageLiteral(resourceName: "pay"), target: self, selector: #selector(payButtonClicked), reverse: true)
        navigationItem.rightBarButtonItem = payButton
        UserData.shared.payments = generateHistory()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        balance.text = "Balance: " + UserData.shared.balanse.description + "€"
        animateTable()
    }
    
    func payButtonClicked() {
        performSegue(withIdentifier: "pay", sender: nil)
    }
}

extension HomeViewController: UITableViewDelegate {}

extension HomeViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return UserData.shared.payments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "payment", for: indexPath) as! PaymentTableViewCell
        cell.payment = UserData.shared.payments[indexPath.row]
        
        return cell
    }
}

extension HomeViewController: PaymentHistory {
    func generateHistory() -> [Payment] {
        let names = ["Joel", "Bill", "Bo", "Mark", "Bob", "James", "Will", "Woo", "Sid"]
        var payments = [Payment]()
        for i in 0..<names.count {
            let direction: Payment.Direction = i%2 == 0 ? .from : .to
            let date = getDate()
            let money = arc4random_uniform(12345).description
            let payment = Payment(date: date, direction: direction, name: names[i], money: money)
            payments.append(payment)
        }
        return payments
    }
    
    func getDate() -> String {
        let date = Date().randomDateInYearOfDate()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy hh:mm:ss"
        return formatter.string(from: date)
    }
}

extension HomeViewController: AnimateTable {
    func animateTable() {
        tableView.reloadData()
        
        let tableHeight = tableView.bounds.size.height
        
        for cell in tableView.visibleCells {
            cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
        }
        
        var index = 0
        
        for cell in tableView.visibleCells {
            UIView.animate(withDuration: 1.5, delay: 0.05 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: [], animations: {
                cell.transform = CGAffineTransform(translationX: 0, y: 0);
            }, completion: nil)
            
            index += 1
        }
    }
}
