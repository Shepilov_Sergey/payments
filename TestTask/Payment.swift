//
//  Payment.swift
//  TestTask
//
//  Created by Sergey on 2/15/17.
//  Copyright © 2017 Sergey Shepilov. All rights reserved.
//

import Foundation

class Payment {
    var date: String
    var direction: Direction
    var name: String
    var money: String
    
    enum Direction {
        case from
        case to
    }
    
    init(date: String, direction: Direction, name: String, money: String) {
        self.date = date
        self.direction = direction
        self.name = name
        self.money = money
    }
}
