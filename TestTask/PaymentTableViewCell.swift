//
//  PaymentTableViewCell.swift
//  TestTask
//
//  Created by Sergey on 2/15/17.
//  Copyright © 2017 Sergey Shepilov. All rights reserved.
//

import UIKit

class PaymentTableViewCell: UITableViewCell {

    @IBOutlet weak var money: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var direction: UILabel!
    @IBOutlet weak var date: UILabel!
    
    var payment: Payment? {
        didSet {
            money.text = payment!.money + "€"
            name.text = payment?.name
            date.text = payment?.date
            
            switch payment!.direction {
            case .from:
                direction.text = "From"
            case .to:
                direction.text = "To"
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
