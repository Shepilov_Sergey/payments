//
//  PaymentViewController.swift
//  TestTask
//
//  Created by Sergey on 2/15/17.
//  Copyright © 2017 Sergey Shepilov. All rights reserved.
//

import UIKit

protocol PaymentNotifications {
    func observeKeyboardNotifications()
    func keyboardShown(notification: NSNotification)
    func keyboardHidden(notification: NSNotification)
}

class PaymentViewController: UIViewController {
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var moneyToSend: UITextField!
    @IBOutlet weak var receiverName: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        let payButton = UIBarButtonItem(title: "Pay", style: .plain, target: self, action: #selector(payButtonPressed))
        navigationItem.rightBarButtonItem = payButton
        hideKeyboardWhenTappedAround()
        observeKeyboardNotifications()
    }
    
    func payButtonPressed() {
        guard let name = receiverName.text, name != "" else {
            receiverName.shake()
            return
        }
        
        guard let money = moneyToSend.text, money != "" else {
            moneyToSend.shake()
            return
        }
        makePayment(to: name, money: money)
        showPopup(title: "Payment Successfully completed", text: "\(money)€ was send to \(name).")
    }
    
    func makePayment(to name: String, money: String) {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy hh:mm:ss"
        let currentDate = formatter.string(from: date)
        let payment = Payment(date: currentDate, direction: .to, name: name, money: money)
        
        UserData.shared.balanse -= Int(money)!
        UserData.shared.payments.append(payment)
    }
    
    func showPopup(title: String, text: String) {
        let popup = UIAlertController(title: title, message: text, preferredStyle: .alert)
        let button = UIAlertAction(title: "Ok", style: .default, handler: nil)
        popup.addAction(button)
        present(popup, animated: true, completion: nil)
    }
}

extension PaymentViewController: PaymentNotifications {
    func observeKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardShown(notification:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardHidden(notification:)), name: .UIKeyboardWillHide, object: nil)
    }
    
    func keyboardShown(notification: NSNotification) {
        var userInfo = notification.userInfo!
        var keyboardFrame = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = view.convert(keyboardFrame, from: nil)
        var contentInset = scrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height
        scrollView.contentInset = contentInset
        scrollView.isScrollEnabled = true
        scrollView.setContentOffset(CGPoint(x: 0, y: keyboardFrame.size.height / 2), animated: true)
    }
    
    func keyboardHidden(notification: NSNotification) {
        scrollView.contentInset = .zero
        scrollView.isScrollEnabled = false
    }
}
