//
//  UIBarButtonItem+Extension.swift
//  TestTask
//
//  Created by Sergey on 2/15/17.
//  Copyright © 2017 Sergey Shepilov. All rights reserved.
//

import Foundation
import UIKit

extension UIBarButtonItem {
    convenience init(title: String, image: UIImage, target: Any, selector: Selector, reverse: Bool) {
        let button = UIButton(type: .system)
        let imageSize = CGSize(width: 22, height: 22)
        button.setImage(image.scale(to: imageSize), for: .normal)
        let titleString = reverse ?  title + " " : " " + title
        button.setTitle(titleString, for: .normal)
        button.addTarget(target, action: selector, for: .touchUpInside)
        
        if reverse {
            button.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            button.titleLabel?.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            button.imageView?.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        }
        button.sizeToFit()
        
        self.init(customView: button)
    }
}
