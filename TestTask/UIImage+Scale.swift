//
//  UIImage+Extension.swift
//  TestTask
//
//  Created by Sergey on 2/15/17.
//  Copyright © 2017 Sergey Shepilov. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    func scale(to size: CGSize) -> UIImage {
        if self.size != size {
            UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
            self.draw(in: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(size.width), height: CGFloat(size.height)))
            let newImage: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            return newImage!
        } else { return self }
    }
}
