//
//  UIView+Shake.swift
//  TestTask
//
//  Created by Sergey on 2/16/17.
//  Copyright © 2017 Sergey Shepilov. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    func shake() {
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.07
        animation.repeatCount = 3
        animation.autoreverses = true
        animation.fromValue = [center.x - 5, center.y]
        animation.toValue = [center.x + 5, center.y]
        layer.add(animation, forKey: "position")
    }
}
